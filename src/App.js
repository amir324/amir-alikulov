import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import uuid from "uuid"

class App extends Component {


  state = {
    name: "",
    region: ""
  }
  
  // TODO: change this implementation from static to dynamic
  buildLocationRows(locations) {
   
    return Object.values(locations).reverse().map(i=>{
        return  <div key={uuid.v4()} className={"results_item"}><div>{i.name}</div><div>{i.region}</div></div>  
      })
      
  };

  onChangeHandler = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }




  // TODO: change the implementation of the add_location button to retrieve the name and region via form input elements
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>

      <div className="input_container">
        <input className="input" type="text" name="name" placeholder="Name" onChange={this.onChangeHandler} value={this.state.name}/>
        <input className="input"type="text" placeholder="Region" name="region" onChange={this.onChangeHandler}  value={this.state.region} />
      </div>

      <button id="add_location" className="button"
        onClick={() => this.props.addLocation({ name: this.state.name, region: this.state.region })}
        >
        
        Add Name and Region
      </button>

      <div className="header">Results</div>
      <div className="results">

      { this.buildLocationRows(this.props.locations) }
      </div>
         
        
    
      </div>
    );
  }
}

export default App;
